<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{
    public function getPfpbyId($id){
        $myId = User::find($id);
        if (is_null($myId)) {
            return response()->json('Data not found', 404);
        }
        $pfp = User::select('profilePic')
        ->where('id', $myId->id)
        ->first();
        //ddd($pfp);
        return response()->file($pfp->profilePic);
        //return response()->download($pfp->profilePic);
    }

    public function getIdByLogin($email, $pwd){
        $hashedPassword = User::select('password')
        ->where('email', $email)
        ->first();
        if(Hash::check($pwd, $hashedPassword->password)){
            $userId = User::select('id')
            ->where('email', $email)
            ->first();
            if(is_null($userId)){
                return response()->json('Data not found' . $email . $pwd, 404);
            }
            return response()->json($userId->id, 200);
        }
    }

    public function storeScoreFromId($id, $newscore){

        $user = User::select('score')
        ->where('id', $id)
        ->first();
        if(is_null($user)){
            return response()->json("User not found", 404);
        }

        DB::update('update users set score = ? where id = ?', [$newscore, $id]);

        return response()->json("succes! new score is " . $newscore, 200);
    }
}
