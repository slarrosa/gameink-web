<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    function Ranking()
    {
        $scores = User::select('name', 'score')
            ->orderBy('score', 'desc')
            ->get()->toArray();
            //ddd($scores);
        return view('welcome', compact('scores'));
    }

    function photo(Request $request){
        $input = $request->all();
        $pfpData = $input['profilePic'];
        if($request->hasFile('profilePic')){
            $destination_path = 'public/images';
            $image = $request->file('profilePic');
            $image_name = $request->file('profilePic')->getClientOriginalName();
            $path = $request->file('profilePic')->storeAs($destination_path,$image_name);
            $input['image'] = $image_name;
            $whereIsImageStored = 'C:\Users\sergi\Documents\Laravel\gameink-web\storage\app\public\images\\' . $image_name;
        }

        //ddd($request->file('profilePic'));
        $myId = $request->user()->id;
        //$myName = $request->user()->name;
        DB::update('update users set profilePic = ? where id = ?', [$whereIsImageStored, $myId]);

        return view('/home');
    }
}
